export class Stack<T> {
  private stac: Array<T> = []; // Can also use linkedlist

  push(data: T) {
    this.stac = this.stac || [];
    this.stac.push(data)
  }

  pop() {
    this.stac.pop();
  }

  getFirstElement(): T {
    return this.stac[0];
  }

  getLastElement(): T {
    return this.stac[this.stac.length - 1];
  }

  getStackLength(): number {
    return this.stac ? this.stac.length : 0;
  }

  getElementAtIndex(index: number): T {
    return this.stac[index];
  }

  map(callBack: Function): Stack<Object> {
    const STACK  = new Stack<Object>();
    this.stac.map((val, index, stac) => {
      return callBack(val, index, stac);
    })
    .forEach((val) => STACK.push(val));

    return STACK;
  }

  length(): number {
    return this.stac.length;
  }
}