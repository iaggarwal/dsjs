import { Queue } from './quque.service';

export class BinarySearchTree<T> {
  root: BSTNode<T>;

  depth: number;
  size: number;

  constructor() {}

  add(value: T) {
    let currentNode = this.root;
    const node = new BSTNode<T>(value);

    if (!this.root) {
      this.root = node;
      return node;
    } else {
      let looper = true;
      while (looper) {
        if (node.data < currentNode.data) {
          if (currentNode.left) {
            currentNode = currentNode.left;
          } else {
            currentNode.left = node;
            looper = false;
          }
        } else {
          if (currentNode.right) {
            currentNode = currentNode.right;
          } else {
            currentNode.right = node;
            looper = false;
          }
        }
      }
    }
  }

  remove(node: BSTNode<T>) {}

  depthFirstInorderTraversal(callBackFn: (value: T) => void) {
    if (!this.root) {
      throw new Error('Empty tree cannot be traversed');
    }

    function traverse(node: BSTNode<T>) {
      if (node.left) {
        traverse(node.left);
      }
      callBackFn(node.data);
      if (node.right) {
        traverse(node.right);
      }
    }

    traverse(this.root);
  }

  depthFirstPreorderTraversal(callBackFn: (value: T) => void) {
    if (!this.root) {
      throw new Error('Empty tree cannot be traversed');
    }

    function traverse(node: BSTNode<T>) {
      callBackFn(node.data);
      if (node.left) {
        traverse(node.left);
      }
      if (node.right) {
        traverse(node.right);
      }
    }

    traverse(this.root);
  }

  depthFirstPostorderTraversal(callBackFn: (value: T) => void) {
    if (!this.root) {
      throw new Error('Empty tree cannot be traversed');
    }

    function traverse(node: BSTNode<T>) {
      if (node.left) {
        traverse(node.left);
      }
      if (node.right) {
        traverse(node.right);
      }
      callBackFn(node.data);
    }

    traverse(this.root);
  }

  breadthFirstTraversal(callBackFn: (value: T) => void) {
    if (!this.root) {
      throw new Error('Empty tree cannot be traversed');
    }

    let queue: Queue<BSTNode<T>> = new Queue<BSTNode<T>>();
    queue.push(this.root);
    while (!queue.isEmpty()) {
      const node = queue.pop();
      callBackFn(node.data);
      if (node.left) {
        queue.push(node.left);
      }

      if (node.right) {
        queue.push(node.right);
      }
    }
  }
}

export class BSTNode<T> {
  data: T;
  left: BSTNode<T>;
  right: BSTNode<T>;
  level: number;

  constructor(data: T) {
    this.data = data;
  }
}

export interface Comparator {
  isGreaterThan: (value: Object) => boolean;
}
