import { Queue } from './quque.service';

export class LRUCache<T> {
  private cacheHashMap: { [key: string]: T };
  private hashList: Queue<string>;
  private cacheSize: number;

  constructor(cacheSize: number) {
    this.cacheSize = cacheSize;
    this.hashList = new Queue<string>();
    this.cacheHashMap = {};
  }

  add(key: string, data: T) {
    if (this.cacheHashMap[key]) {
      this.hashList.remove(key); 
    }

    if(this.hashList.length === this.cacheSize) {
      const hashKey = this.hashList.pop();
      delete this.cacheHashMap[hashKey];
    }
    this.cacheHashMap[key] = data;
    this.hashList.push(key);
  }

  accessCache(key: string): T {
    if(this.cacheHashMap[key]) {
      this.hashList.remove(key);
      this.hashList.push(key);
      return this.cacheHashMap[key];
    }
  }
}
